package com.avatar.examen.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PokemonController {
 
    @GetMapping("/list")
    public String List(){
        return "Pokemon/Listar.html";
    }
}
