package com.avatar.examen.Dto.PokemonDTO.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.*;

import com.avatar.examen.Dto.PokemonDTO.List.Internal.Pokemon;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PokemonResponse {
    private List<Pokemon> results;
}
