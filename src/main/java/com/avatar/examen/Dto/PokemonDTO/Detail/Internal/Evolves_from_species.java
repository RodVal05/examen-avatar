package com.avatar.examen.Dto.PokemonDTO.Detail.Internal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Evolves_from_species {
    private String name;
}
