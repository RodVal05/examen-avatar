package com.avatar.examen.Dto.PokemonDTO.Detail.Internal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PokedexNumber {
    private int entry_number;
    private Pokedex pokedex;
}
