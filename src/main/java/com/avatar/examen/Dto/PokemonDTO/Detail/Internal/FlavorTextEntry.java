package com.avatar.examen.Dto.PokemonDTO.Detail.Internal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlavorTextEntry {
    private String flavor_text;
    private Languaje language;
    private Version version;
}
