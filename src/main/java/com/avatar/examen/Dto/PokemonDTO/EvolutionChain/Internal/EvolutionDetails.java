package com.avatar.examen.Dto.PokemonDTO.EvolutionChain.Internal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EvolutionDetails {
    private String gender;
    private String held_item;
    private Item item;
    private String known_move;
    private String known_move_type;
    private String location;
    private String min_affection;
    private String min_beauty;
    private String min_happiness;
    private String min_level;
    private Boolean needs_overworld_rain;
    private String party_species;
    private String party_type;
    private String relative_physical_stats;
    private String time_of_day;
    private String trade_species;
    private Trigger trigger;
    private Boolean turn_upside_down;
    
}
