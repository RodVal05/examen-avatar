package com.avatar.examen.Dto.PokemonDTO.EvolutionChain;

import com.avatar.examen.Dto.PokemonDTO.EvolutionChain.Internal.Chain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PokemonEvChainResponse {
    private String baby_trigger_item;
    private Chain chain;
    private Long id;
}
