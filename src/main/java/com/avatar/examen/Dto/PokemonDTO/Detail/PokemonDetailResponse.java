package com.avatar.examen.Dto.PokemonDTO.Detail;

import java.util.List;

import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.Color;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.EggGroup;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.EvChain;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.Evolves_from_species;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.FlavorTextEntry;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.Habitat;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.PokedexNumber;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.Shape;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.Varieties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PokemonDetailResponse {
    private int base_happiness;
    private int capture_rate;
    private Color color;
    private List<EggGroup> egg_groups;
    private EvChain evolution_chain;
    private Evolves_from_species evolves_from_species;
    private List<FlavorTextEntry> flavor_text_entries;
    private int gender_rate;
    private Habitat habitat;
    private Boolean has_gender_differences;
    private int hatch_counter;
    private Boolean is_baby;
    private Boolean is_legendary;
    private Boolean is_mythical;
    private String name;
    private List<PokedexNumber> pokedex_numbers;
    private Shape shape;
    private List<Varieties> varieties;

}
