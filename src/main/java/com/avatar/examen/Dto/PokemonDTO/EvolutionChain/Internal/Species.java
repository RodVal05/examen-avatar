package com.avatar.examen.Dto.PokemonDTO.EvolutionChain.Internal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Species {
    private String name;   
}
