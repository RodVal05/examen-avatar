package com.avatar.examen.Dto.PokemonDTO.EvolutionChain.Internal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Chain {
    private List<EvolutionDetails> evolution_details;
    private List<EvolvesTo> evolves_to;
    private Boolean is_baby;
    private Species species;
}
