package com.avatar.examen.Dto.PokemonDTO.List.Internal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon {
    private String name;
    private String url;
}
