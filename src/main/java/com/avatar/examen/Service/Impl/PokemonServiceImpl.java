package com.avatar.examen.Service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.avatar.examen.Dto.PokemonDTO.Detail.PokemonDetailResponse;
import com.avatar.examen.Dto.PokemonDTO.Detail.Internal.FlavorTextEntry;
import com.avatar.examen.Service.PokemonService;

@Service
public class PokemonServiceImpl implements PokemonService {

    
    public PokemonDetailResponse FiltrarIdioma(String Languaje, PokemonDetailResponse PokedetailResponse) {
        List<FlavorTextEntry> flavorTextEntry= new ArrayList<FlavorTextEntry>();
        for(FlavorTextEntry fte: PokedetailResponse.getFlavor_text_entries()){
            if(fte.getLanguage().getName().equals(Languaje)){
                flavorTextEntry.add(fte);
            }
        }
        PokedetailResponse.setFlavor_text_entries(flavorTextEntry);
        return PokedetailResponse;
    }


}
