package com.avatar.examen.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

@ExceptionHandler(Exception.class)
public ResponseEntity<Object> handleException(Exception ex) {
    // Obtener el mensaje de error de la excepción
    String errorMessage = ex.getMessage();
    String response;
    
    // Verificar si el mensaje de error está vacío o es nulo
    if (errorMessage == null || errorMessage.isEmpty()) {
        response = "Se produjo un error al comunicarse con la api.";
    }else{
        response = "Se produjo un error al comunicarse con la api.|"+errorMessage;
    }
    
    // Devolver una respuesta de error con el mensaje de error original
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(response);
}

}