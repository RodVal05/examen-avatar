package com.avatar.examen.RestController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.avatar.examen.Dto.PokemonDTO.Detail.PokemonDetailResponse;
import com.avatar.examen.Dto.PokemonDTO.EvolutionChain.PokemonEvChainResponse;
import com.avatar.examen.Dto.PokemonDTO.List.PokemonResponse;
import com.avatar.examen.Dto.PokemonDTO.List.Internal.Pokemon;
import com.avatar.examen.Service.PokemonService;

@RestController
@RequestMapping("poke/v1")
public class PokemonRestController {
    @Autowired
    private PokemonService pokemonService;

    @GetMapping("/list")
    @Cacheable("pokemonList")
    public List<Pokemon> getPokemon() {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://pokeapi.co/api/v2/pokemon-species?limit=1010";
        PokemonResponse response = restTemplate.getForObject(apiUrl, PokemonResponse.class);
        return response.getResults();
    }

    @GetMapping("/detail/{id}")
    public PokemonDetailResponse detailPokemon(@PathVariable("id")Long id) {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://pokeapi.co/api/v2/pokemon-species/"+id;
        PokemonDetailResponse response = restTemplate.getForObject(apiUrl, PokemonDetailResponse.class);
        response=pokemonService.FiltrarIdioma("es", response);
        return response;
    }

    @GetMapping("/evolution-chain")
    public PokemonEvChainResponse EvolutionChainPokemon(@RequestParam(defaultValue = "")String url) {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = url;
        PokemonEvChainResponse response = restTemplate.getForObject(apiUrl, PokemonEvChainResponse.class);
        return response;
    }
}
